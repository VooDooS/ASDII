//
//  Application.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 29/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Application.hpp"

#include <SFML/Window/Event.hpp>

#include "states/GameState.hpp"
#include "states/GOverState.hpp"
#include "states/StartState.hpp"

using namespace std;


const sf::Time Application::m_timePerFrame = sf::seconds(1.f / 60.f);

Application::Application()
    : m_window(),
    m_texture(),
    m_resourcesManager(),
    m_player(m_window),
    m_stateStack(State::Context {m_window, m_texture, m_resourcesManager, m_player, m_stats})
{
    cout << "[debug] Application constructor" <<endl;
    
    //Creating window :
    m_window.create(sf::VideoMode(m_resourcesManager.getConfig()->getXRes(), m_resourcesManager.getConfig()->getYRes()), "Alien Space Domination II");
    
    // Creating rendertexture :
    cout << "Max size " << sf::Texture::getMaximumSize() << endl;
    m_texture.create(m_window.getSize().x, m_window.getSize().y);
    m_window.resetGLStates();
    
    // Hiding mouse :
    m_window.setMouseCursorVisible(false);
    m_window.requestFocus();
    
    m_window.setKeyRepeatEnabled(false);
    
    m_window.setVerticalSyncEnabled(false);
    
    
    // Initializing stateStack :
    registerStates();
    
    // Starting the maching :
    m_stateStack.pushState(State::Start);
}


void Application::run() {
    // Starting the clock and then the evenmential loop
    sf::Clock clock;
    sf::Time timeSinceLastUpdate = sf::Time::Zero;
    m_window.setMouseCursorVisible(false);
    
    while (m_window.isOpen())
    {
        sf::Time elapsed = clock.restart();
        timeSinceLastUpdate += elapsed;
        
        while (timeSinceLastUpdate > m_timePerFrame) {
            timeSinceLastUpdate -= m_timePerFrame;
            
            processInputs();
            
            if (m_focused) update(m_timePerFrame);
            
            testClose();
        }
        
        render(elapsed);
    }
}

void Application::render(sf::Time elapsed) {
    m_texture.clear(sf::Color::Transparent);
    m_stateStack.draw();
    m_texture.display();
    
    m_window.clear(sf::Color::Black);
    m_window.draw(sf::Sprite(m_texture.getTexture()));
    
    drawFPS(elapsed);
    m_window.display();
    
}

void Application::registerStates() {
    m_stateStack.registerState<StartState>(State::Start);
    m_stateStack.registerState<GameState>(State::Game);
    m_stateStack.registerState<GOverState>(State::GameOver);
}

void Application::processInputs() {
    sf::Event event;
    while(m_window.pollEvent(event)) {
        if (event.type == sf::Event::Closed)
            m_window.close();
        
        if (event.type == sf::Event::GainedFocus)
            m_focused = true;
        
        if (event.type == sf::Event::LostFocus)
            m_focused = false;
        
        if(m_focused) m_stateStack.handleEvent(event);
        testClose();
    }
}

void Application::update(sf::Time dt) {
    m_stateStack.update(dt);
}

void Application::testClose() {
    if(m_stateStack.isEmpty()) m_window.close();
}

void Application::drawFPS(sf::Time elapsed) {
    int t = 1./ (double)elapsed.asSeconds();
    
    sf::Text text;
    sf::Font font;
    if (!font.loadFromFile(Resource::resourcePath() + "fonts/FantasqueSansMono-Regular.ttf")) {
    }
    text.setFont(font);
    text.setColor(sf::Color::Yellow);
    text.setString(to_string(t));
    m_window.draw(text);
}