//
//  Universe.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 10/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Universe.hpp"

#include <cmath>
#include <iostream>

#include "entities/Tiles.hpp"

using namespace std;



Universe::Universe(State::Context c) :
    m_win(*c.win), m_rtext(*c.texture), m_view(c.win->getDefaultView()) {
    m_unzoomedViewSize = m_view.getSize();
    buildRender();
}

void Universe::update(sf::Time dt) {
    // Collision analysis :
    m_renderGraph.testCollisions();
    
    // Command and conquer :
    sendCommands(dt);
        
    // Updating graph :
    m_renderGraph.update(dt);

    // Zoom is normalized between 1 and 2 with regards to the window's diagonal
    auto mp = m_player->getPosition();
    auto mc = m_win.mapPixelToCoords(sf::Vector2i(m_cursor->getPosition()), m_view);
    m_zoom = 1.f + ((float) sqrt(pow(mp.x - mc.x, 2.f) + pow(mp.y - mc.y, 2.f))
                / (float) sqrt(pow(m_win.getSize().x, 2.f) + pow(m_win.getSize().y, 2.f))) / 5.f;

    m_view.setCenter((mp + mc) / 2.f);
    m_view.setSize(m_unzoomedViewSize * m_zoom * 1.2f);

}

void Universe::draw() {
    m_rtext.setView(m_view);
    m_rtext.draw(m_renderGraph);
    
    m_rtext.setView(m_win.getDefaultView());
    m_rtext.draw(m_renderGui);
}

sf::View& Universe::getView() {
    return m_view;
}

CommandsQueue& Universe::getCommandsQueue() {
    return m_coqueue;
}

array<RenderNode*, Universe::LayerCount>& Universe::getRenderlayers() {
    return m_renderLayers;
}

void Universe::buildRender() {
    // Creating layers :
    for(size_t i = 0; i < LayerCount; i++) {
        RenderNode::rn_ptr layer(new RenderNode());
        m_renderLayers[i] = layer.get();
        
        m_renderGraph.attachChild(move(layer));
    }
    
    // Adding Cursor :
    unique_ptr<Cursor>  cursor(new Cursor(m_win));
    m_cursor = cursor.get();
    m_renderGui.attachChild(move(cursor));

    // Adding background :
    m_renderLayers[Background]->attachChild(unique_ptr<Tiles>(new Tiles()));
    
    // Adding player :
    unique_ptr<Creature> player(new Creature(VectorF(1.f, 1.f, 500.f), m_win,m_view, *m_cursor, true));
    m_player = player.get();
    m_renderLayers[Floor]->attachChild(move(player));
    
    
    // Adding an ennemy :
    m_renderLayers[Floor]->attachChild(unique_ptr<Creature>(new Creature(VectorF(1.f, 1.f, 300.f), m_win,m_view, *m_cursor, false, sf::Vector2f(150.f, 200.f))));
    
    // Mouse and cursor in center :
    auto center = sf::Vector2i(m_win.getSize() / 2u);
    sf::Mouse::setPosition(center, m_win);
    m_cursor->setPosition(sf::Vector2f(center));
    
}

void Universe::sendCommands(sf::Time dt) {
    while(!m_coqueue.empty()) {
        auto c = m_coqueue.pop();
        m_renderGraph.onCommand(c, dt);
        m_renderGui.onCommand(c, dt);
    }
}