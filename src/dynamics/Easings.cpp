#include "Easings.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <functional>
#include <iostream>
#include <SFML/Graphics.hpp>
#include "Vector.hpp"

using namespace std;


const array<Easings::funParam, Easings::Kind::ENUM_COUNT> Easings::ease_tab = {
    // CONST
    [](sf::Time, sf::Time, VectorF v, float, bool){ return v; },
    //SIN
    [](sf::Time t, sf::Time easy_time, VectorF v, float, bool in ){
        double time = t.asMilliseconds() / (double)1000;
        double etime = easy_time.asMilliseconds() / (double)1000;
        //cout << t.asMilliseconds() << "-" << easy_time.asMilliseconds() << endl;
        if(time < etime){
            if(in)
                return v*(1.f + (float) sin(((M_PI / etime)*time) - (M_PI/2))) / 2.f;
            else
                return v*(1.f + (float) sin(((M_PI / etime)*time) + (M_PI/2))) / 2.f;
        }
        else {
            if(in) return v;
            else return VectorF(0.f, 0.f);
        }
    }
};


Easings::Easings(Kind e, sf::Time t, float p)
: m_e(ease_tab[e]), m_ease_time(t), m_p(p), m_in(true) {
    
}

VectorF Easings::operator()(sf::Time dt, VectorF v) {
    if(m_clock < m_ease_time) m_clock += dt;
    return m_e(m_clock, m_ease_time, v, m_p, m_in);
}

void Easings::reBind(sf::Time t) {
    reBind(t, m_p);
}
void Easings::reBind(sf::Time t, float p) {
    m_ease_time = t;
    m_p = p;
}


void Easings::in(bool join) {
    // Continuity ?
    if(join) m_clock = m_ease_time - m_clock;
    else m_clock = sf::Time::Zero;
    
    m_in = true;;
}


void Easings::out(bool join) {
    // Continuity ?
    if(join) m_clock = m_ease_time - m_clock;
    else m_clock = sf::Time::Zero;
    
    m_in = false;;
}
