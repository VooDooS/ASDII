//
//  Easings.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 09/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <array>
#include <functional>
#include <SFML/System/Time.hpp>
#include <stdio.h>

#include "Vector.hpp"


class Easings {
public:
    using fun = std::function<VectorF(sf::Time, VectorF, bool)>;
    using funParam = std::function<VectorF(sf::Time, sf::Time, VectorF, float, bool)>;
    enum {
        IN = true,
        OUT = false
    };
    enum Kind {
        CONST,
        SIN,
        ENUM_COUNT
    };
    
    Easings(Kind, sf::Time, float = 0.f);
    
    VectorF operator()(sf::Time, VectorF);
    
    void reBind(sf::Time);
    void reBind(sf::Time, float);
    
    void in(bool = false);
    void out(bool = true);
    
    
private:
    static const std::array<funParam, ENUM_COUNT> ease_tab;
    
    float m_p;
    bool m_in;
    sf::Time m_ease_time, m_clock = sf::Time::Zero;
    funParam m_e;
};
