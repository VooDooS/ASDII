//
//  CollidableCircle.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 26/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "CollidableCircle.hpp"

#include "../Vector.hpp"


CollidableCircle::CollidableCircle(float size) : m_diameter(size) {}

bool CollidableCircle::isColliding(const Collidable &c) const {
    if(const auto* d = static_cast<const CollidableCircle*>(&c)) {
        float d1 = d->getDiameter();
        float d2 = m_diameter;
        auto p1 = dynamic_cast<const Entity*>(d)->getPosition();
        auto p2 = dynamic_cast<const Entity*>(this)->getPosition();
        
        return (VectorF::computeNorm(p1 - p2) <= d1 + d2);
    }
    else {
        return false;
    }
}

float CollidableCircle::getDiameter() const {
    return m_diameter;
}