//
//  CollidableCircle.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 26/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <stdio.h>

#include "Collidable.hpp"
#include "../../entities/Entity.hpp"

class CollidableCircle : public Collidable {
private:
    float m_diameter;
public:
    CollidableCircle(float);
    bool isColliding(const Collidable&) const;
    
    float getDiameter() const;
};