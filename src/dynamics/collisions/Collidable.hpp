//
//  Collidable.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 26/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <SFML/System/Vector2.hpp>
#include <stdio.h>
#include <set>


class Entity;
class Collidable {
public:
    virtual bool isColliding(const Collidable&) const = 0;
    
    // The next one returns true if the entity must be destroyed (snif):
    virtual bool touchedBy(const Entity&) = 0;
};
