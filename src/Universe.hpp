//
//  Universe.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 10/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <array>
#include <SFML/Graphics.hpp>
#include <stdio.h>

#include "entities/Creature.hpp"
#include "entities/Cursor.hpp"
#include "entities/Tiles.hpp"
#include "renderGraph/CommandsQueue.hpp"
#include "renderGraph/RenderNode.hpp"
#include "states/State.hpp"

class Universe : private sf::NonCopyable {
public:
    enum Layer {
        Background,
        Floor,
        Air,
        LayerCount
    };

private:
    sf::RenderWindow& m_win;
    sf::RenderTexture& m_rtext;
    sf::View m_view;
    RenderNode m_renderGraph;
    std::array<RenderNode*, LayerCount> m_renderLayers;
    RenderNode m_renderGui;
    CommandsQueue m_coqueue;
    
    Creature* m_player;
    Cursor* m_cursor;
    float m_zoom;
    sf::Vector2f m_unzoomedViewSize;
    
public:
    explicit Universe(State::Context);
    
    void update(sf::Time);
    void draw();
    
    sf::View& getView();
    CommandsQueue& getCommandsQueue();
    std::array<RenderNode*, LayerCount>& getRenderlayers();
    
private:
    //void addToRender();
    void buildRender();
    void sendCommands(sf::Time);
    
};
