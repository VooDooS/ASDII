//
//  RTile.cpp
//  AlienSpaceDomination II
//
//  Created by Ulysse Gérard on 27/01/2016.
//  Copyright © 2016 Devilstructoïds. All rights reserved.
//

#include "RTile.hpp"

using namespace std;

const list<string> RTile::m_mandatory_fields {"name", "index", "h_size", "v_size", "collide"};

RTile::RTile(string tsn, json j) : Resource(string(j["name"].get<string>()), j, m_mandatory_fields) {
    m_tileset_name = tsn;
    m_name = j.at("name");
    m_size = sf::Vector2u(j.at("h_size"), j.at("v_size"));
    
    try{
        for(unsigned i = 0; i < m_size.x * m_size.y; i++){
            array<int, 4> a;
            for(int ii = 0; ii < 4; ii++) {
                a[ii] = j.at("collide").at(i).at(ii).get<int>();
            }
            m_collide.push_back(a);
        }
    } catch (const exception& e) {
        cout << "Collisions incohérentes pour la tuile " + getFullName() + ". (" << e.what() << ")" << endl;
    }
}

RTile::~RTile(){
    cout << "Tile " << m_name << " deleted." << endl;
}

string RTile::getFullName() const {
    return m_tileset_name + "." + m_name;
}