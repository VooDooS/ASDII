#include "RConfig.hpp"

using namespace std;


RConfig::RConfig() : Resource("config.json")
{
}


RConfig::~RConfig()
{
    cout << "[debug] RConfig Destructor" << endl;
}


const unsigned int RConfig::getXRes() const {
    return m_infos["res_x"];
}

const unsigned int RConfig::getYRes() const {
    return m_infos["res_y"];
}

