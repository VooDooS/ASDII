//
//  Tile.hpp
//  AlienSpaceDomination II
//
//  Created by Ulysse Gérard on 27/01/2016.
//  Copyright © 2016 Devilstructoïds. All rights reserved.
//

#pragma once

#include <array>
#include <list>
#include <SFML/Graphics.hpp>
#include <stdio.h>
#include <vector>

#include "json.hpp"
using json = nlohmann::json;


#include "Resource.hpp"

class RTile : public Resource {
public:
    using NamedTile = std::pair<std::string, std::unique_ptr<RTile>>;
    using Tiles = std::map<std::string, std::unique_ptr<RTile>>;
private:
    static const std::list<std::string> m_mandatory_fields;
    
    std::string m_name, m_tileset_name;
    std::vector<std::array<int, 4>> m_collide;
    sf::Vector2u m_size;
    
public:
    RTile(std::string, json);
    ~RTile();
    
    std::string getFullName() const;

private:
    void loadData() {}
    void unloadData() {}
};

