//
//  ResourcesManager.cpp
//  AlienSpaceDomination II
//
//  Created by Ulysse Gérard on 22/01/2016.
//  Copyright © 2016 Devilstructoïds. All rights reserved.
//

#include "RConfig.hpp"
#include "ResourcesManager.hpp"
using namespace std;


ResourcesManager::ResourcesManager() {
    cout << "[debug] RM constructor" <<endl;

    // Loading config :
    newResource("config", new RConfig());

    // Loading TileSets :
    loadTileSets("tiles.json");

    // Loading world :
    newResource("world", new RWorld(this));

    // Loading all data !
    loadAllResourcesData();
}

ResourcesManager::~ResourcesManager(){
    cout << "[debug] RM destructor" <<endl;
    
    //Deleting everything :
    m_tilesets.clear();
    
    for(Resource::Resources::iterator it = m_resources.begin(); it != m_resources.end(); ++it) {
       delete it->second;
    }

}

const RConfig* ResourcesManager::getConfig() const {
    return dynamic_cast<RConfig*>(m_resources.at("config"));
}

const RTileSet& ResourcesManager::getTileSet(std::string tn) const
{
    return *m_tilesets.at(tn);
}

const RWorld* ResourcesManager::getWorld() const {
    return dynamic_cast<RWorld*>(m_resources.at("world"));
}

void ResourcesManager::newResource(const string n, Resource *r) {
    m_resources.insert(Resource::NamedResource(n, r));
}

void ResourcesManager::loadTileSets(const string fn)
{
    json j = Resource::loadResourceInfos(fn);

    for (json::iterator it = j.begin(); it != j.end(); ++it) {
        try {
            string n = it->at("name");
            m_tilesets.insert(RTileSet::NamedTileSet(n, unique_ptr<RTileSet>(new RTileSet(n, *it))));
        } catch (out_of_range e) {
            cout << "Field name missing in " << fn << " tileset." << endl;
        }
    }
}

void ResourcesManager::loadAllResourcesData() {

    for (RTileSet::RTileSets::iterator it = m_tilesets.begin(); it != m_tilesets.end(); ++it)
       it->second->loadData();

    for (Resource::Resources::iterator it = m_resources.begin(); it != m_resources.end(); ++it)
        it->second->loadData();
}