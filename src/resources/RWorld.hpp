#pragma once

#include <map>

#include "Cell.hpp"
#include "Resource.hpp"

class ResourcesManager;
class RWorld :
    public Resource
{
    friend class ResourcesManager;
private:
    static const std::list<std::string> m_mandatory_fields;

    ResourcesManager *m_rm;
    Cells m_cells;

public:
    RWorld(ResourcesManager*);
    ~RWorld();
private:
    void loadData() {}
    void unloadData() {};
};

