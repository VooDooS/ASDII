#include "ResourcesManager.hpp"
#include "RWorld.hpp"

using namespace std;


const list<string> RWorld::m_mandatory_fields{ "content" };

RWorld::RWorld(ResourcesManager *rm) : Resource("world.json", m_mandatory_fields), m_rm(rm)
{
    //For each case in content we link the Tile object :
    json content = m_infos.at("content");
    for (json::iterator it = content.begin(); it != content.end(); ++it) {

        try {
            sf::Vector2i coord = sf::Vector2i(it->at("x"), it->at("y"));
            string ts = it->at("tileset");
            string t = it->at("tile");
            try {
                m_cells.insert(CoordCell(Coord(it->at("x"), it->at("y")),
                    Cell(rm->getTileSet(ts).getTile(t))));
            }
            catch (out_of_range e) {
                cout << "Tile " << ts << "." << t << " missing." << endl;
            }
        }
        catch (out_of_range e) {
            cout << "Field missing in " << m_fn << " world content." << endl;
        }
    }
}


RWorld::~RWorld()
{
}
