//
//  ResourcesManager.hpp
//  AlienSpaceDomination II
//
//  Created by Ulysse Gérard on 22/01/2016.
//  Copyright © 2016 Devilstructoïds. All rights reserved.
//
#pragma once

#include <string>

#include "RConfig.hpp"
#include "Resource.hpp"
#include "RTileSet.hpp"
#include "RWorld.hpp"

class ResourcesManager {
private:
    RTileSet::RTileSets m_tilesets;
    Resource::Resources m_resources;

public:
    ResourcesManager();
    ~ResourcesManager();
    
    const RConfig* getConfig() const;
    const RTileSet& getTileSet(std::string) const;
    const RWorld* getWorld() const;
private:
    void newResource(const std::string, Resource*);
    void loadTileSets(const std::string);
    void loadAllResourcesData();
};