#pragma once

#include <SFML/system.hpp>

#include <list>
#include <map>

#include "json.hpp"
using json = nlohmann::json;

class Resource : private sf::NonCopyable
{
    friend class ResourcesManager;

    using NamedResource = std::pair<std::string, Resource*>;
    using Resources = std::map<std::string, Resource*>;

protected:
    std::string m_fn;
    json m_infos;
    
    static const std::list<std::string> m_mandatory_fields;
    
public:
    Resource(std::string, json, std::list<std::string> = m_mandatory_fields, bool = true);
    Resource(std::string, std::list<std::string> = m_mandatory_fields, bool = true);
    virtual ~Resource();

    const json getInfos() const;

    static json loadResourceInfos(std::string);
    static std::string resourcePath();


protected:
    virtual void loadData() = 0;
    virtual void unloadData() = 0;
    
private:
    virtual bool checkInfos(std::list<std::string>, bool) const;
};
