#pragma once

#include <map>
#include <string>
#include <SFML/Graphics.hpp>

#include "Resource.hpp"
#include "RTile.hpp"

class RTileSet :
    public Resource
{
    friend class ResourcesManager;
    using NamedTileSet = std::pair<std::string, std::unique_ptr<RTileSet>>;
    using RTileSets = std::map<std::string, std::unique_ptr<RTileSet>>;
private:
    static const std::list<std::string> m_mandatory_fields;
    
    std::string m_tiles_file;
    sf::Texture* m_texture;
    
    std::string m_name;
    int m_tiles_size;
    
    RTile::Tiles m_tiles;
    
public:
    RTileSet(std::string, json);
    ~RTileSet();

    const RTile& getTile(std::string) const;

private:
    void loadData();
    void unloadData();
};

