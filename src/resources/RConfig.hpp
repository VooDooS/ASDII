#pragma once
#include "Resource.hpp"

class RConfig :
    public Resource
{
public:
    RConfig();
    ~RConfig();
    const unsigned int getXRes() const;
    const unsigned int getYRes() const;
private:
    void loadData() {}
    void unloadData() {};
};

