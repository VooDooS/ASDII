#include "Database.hpp"
#include "Resource.hpp"

#ifdef __APPLE__
#include "CoreFoundation/CoreFoundation.h"
#endif


using namespace std;

list<std::string> const Resource::m_mandatory_fields = {};

Resource::Resource(string fileName, json j, list<string> mfs, bool checkMustPass) : m_fn(fileName), m_infos(j)
{
    checkInfos(mfs, checkMustPass);
}

Resource::Resource(string fileName, list<string> mfs, bool checkMustPass) : Resource(fileName, loadResourceInfos(fileName), mfs, checkMustPass)
{
}


Resource::~Resource(){
    cout << "Resource " << m_fn << " deleted."<< endl;
}

const json Resource::getInfos() const {
    return m_infos;
}

string Resource::resourcePath()
{
#if defined(_WIN32) || defined(_WIN64)
    return "resources/";
#endif
#ifdef __APPLE__
    CFBundleRef mainBundle = CFBundleGetMainBundle();
    CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL(mainBundle);
    char path[PATH_MAX];
    if (!CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, PATH_MAX))
    {
        // error!
    }
    CFRelease(resourcesURL);
    
    chdir(path);
    return string(path) + "/";
#endif
}

json Resource::loadResourceInfos(string fn) {
    return Database::readFromFile(fn);
}

bool Resource::checkInfos(list<string> mfs, bool checkMustPass) const {
    bool flag = true;
    for (list<string>::const_iterator it=mfs.cbegin();
         it != mfs.end(); ++it) {
        try {
            m_infos.at(it->data());
        } catch (out_of_range e) {
            cout << "Field " << it->data() << " missing in " << m_fn << endl;
            flag = false;
        }
    }
    if(checkMustPass && !flag){
        throw runtime_error ("Missing field(s) in " + m_fn);
    }
    return flag;
}