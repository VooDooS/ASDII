#include "RTileSet.hpp"

using namespace std;


const list<std::string> RTileSet::m_mandatory_fields {"name", "tiles_size", "file", "content"};

RTileSet::RTileSet(string n, json j) : Resource(n, j, m_mandatory_fields)
{
    m_name = m_infos.at("name");
    m_tiles_file = m_infos.at("file");
    m_tiles_size = m_infos.at("tiles_size");
    
    //For each tile in content we create a Tile object :
    json content = m_infos.at("content");
    for(json::iterator it = content.begin(); it != content.end(); ++it){
        
        try {
            string n = it->at("name");
            m_tiles.insert(RTile::NamedTile(n, unique_ptr<RTile>(new RTile(m_name, *it))));
        } catch (out_of_range e) {
            cout << "Field name missing in " << m_fn << " tiles content." << endl;
        }
        
    }
    
}


void RTileSet::loadData() {
    // We load texture from file :
    m_texture = new sf::Texture();
    if (m_texture->loadFromFile(Resource::resourcePath() + "tilesets/" + m_tiles_file)) {
        cout << "Texture " << m_tiles_file << " loaded." << m_texture->getSize().x << endl;
    }
}

RTileSet::~RTileSet()
{
    unloadData();
    cout << "Tileset " << m_name << " deleted." << endl;
}

const RTile & RTileSet::getTile(std::string n) const
{
    return *m_tiles.at(n);
}

void RTileSet::unloadData() {
    //Destroying the Tiles map :
    m_tiles.clear();
    
    // Unloading the texture :
    delete m_texture;
}
