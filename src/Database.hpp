#pragma once
#include "json.hpp"
#include<string>

using json = nlohmann::json;

class Database
{
public:
    Database();
    ~Database();

public:
    static json readFromFile(std::string);
    static void saveToFile(std::string, json);
};

