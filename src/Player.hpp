//
//  Player.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 16/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <map>
#include <SFML/System.hpp>
#include <stdio.h>

#include "renderGraph/CommandsQueue.hpp"
#include "Universe.hpp"

class Player {
private:
    
    enum Action {
        MoveUp, MoveLeft, MoveDown, MoveRight
    };
    
    sf::RenderWindow* m_win;

    std::map<sf::Keyboard::Key, Action> m_actionMap;
    std::map<Action, Command> m_commandMap;
    std::map<Action, Command> m_commandOnReleaseMap;
    
public:
    Player(sf::RenderWindow&);
    void handleEvent(const sf::Event&, CommandsQueue&);
    void handleRealtimeInput(CommandsQueue&);
    
private:
    bool isRealTimeAction(Action) const;
};
