#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>

#include "Application.hpp"

int main()
{
    //The Game !
    Application app;
    app.run();

    return 0;
}