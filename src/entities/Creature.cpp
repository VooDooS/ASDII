//
//  Creature.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 09/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Creature.hpp"

#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

using namespace std;
Creature::Creature(VectorF v, sf::RenderWindow &w, sf::View &vi, Cursor& c, bool ip, sf::Vector2f pos)
: Entity(Easings::Kind::SIN, v),
CollidableCircle(30.f),
m_sprite(30.f),
isPlayer(ip),
m_cursor(&c),
m_win(&w),
m_view(&vi)
{
    
    if(ip) m_sprite.setFillColor(sf::Color::Green);
    else m_sprite.setFillColor(sf::Color::Magenta);
    
    // Origin at center of sprite :
    sf::FloatRect bounds = m_sprite.getLocalBounds();
    m_sprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
    
    setPosition(pos);
}

void Creature::computeWalkingAngle() {
    /*for(Dir d : m_lastDirs) {
        cout << d << "; ";
    } cout << endl;*/
    
    
    float a = 0.f;
    int i = 0, j = 0;
    bool change = false;
    Dir first = NB_D;
    for(Dir d : m_lastDirs) {
        if(i == 0) first = d;
        if(i > 1) break;
        switch (d) {
            case U:
                if(first != D) j++;
                break;
            case L:
                if(first != R) {
                    j++;
                    if(first == D) change = true;
                    a -= (float)M_PI_2;
                }
                break;
            case R:
                if(first != L) {
                    j++;
                    a += (float)M_PI_2;
                }
                break;
            case D:
                if(first != U) {
                    j++;
                    if(first == L) change = true;
                    a += (float)M_PI;
                }
                break;
            default:
                break;
        }
        i++;
    }
    if(j != 0) a /= j;
    if(change) a += (float)M_PI;
    m_walkingAngle = a;
}

/*void Creature::changeDirection(sf::Vector2f d) {
    m_lookDirection = d;
    m_velocity.setAngle(m_walkingAngle);
    if(m_relativeWalk) m_velocity += VectorF::computeAngle(d);
    else m_velocity -= (float)M_PI_2;
}*/

void Creature::changeDirection() {
    m_lookDirection = m_win->mapPixelToCoords(sf::Vector2i( m_cursor->getPosition() ), *m_view) - getPosition();
    m_velocity.setAngle(m_walkingAngle);
    if(m_relativeWalk) m_velocity += VectorF::computeAngle(m_lookDirection);
    else m_velocity -= (float)M_PI_2;
}

void Creature::drawThis(sf::RenderTarget &target, sf::RenderStates states) const{
    target.draw(m_sprite, states);
}

Category::Type Creature::getCategory() const {
    if(isPlayer) return Category::PlayerCreature;
    return Category::EnemyCreature;
}


bool Creature::touchedBy(const Entity& c) {
    switch (c.getCategory()) {
        case Category::AllyProjectile:
            if(!isPlayer) {
                setPosition(((float) rand() / (RAND_MAX+1))*800 -400,
                                       ((float) rand() / (RAND_MAX+1))*800 - 400);
            }
            return false;
        case Category::Creature:
        case Category::EnemyCreature:
            // We should not revert but push back !
            cout << "pouet" << endl;
            revertLastTransform();
            return false;
        default:
            return false;
            
    }
}

