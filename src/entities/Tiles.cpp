//
//  Tiles.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 18/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Tiles.hpp"

#include <SFML/Graphics.hpp>

#include "../resources/Resource.hpp"

Tiles::Tiles() : Entity() {
    
}

void Tiles::drawThis(sf::RenderTarget &target, sf::RenderStates states) const{
    sf::Font font;
    font.loadFromFile(Resource::resourcePath() + "fonts/SigmarOne.ttf");
    sf::Text txt;
    txt.setFont(font);
    txt.setColor(sf::Color(50, 50, 50));
    txt.setString("C'est le fond ");
    
    for(float i = -500.f; i < 500.f; i += 30.f) {
        for(float j = -800.f; j < 800.f; j += 250.f) {
            txt.setPosition(j,i);
            target.draw(txt, states);
        }
    }
}