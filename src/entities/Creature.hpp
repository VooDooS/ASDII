//
//  Creature.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 09/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <iostream>
#include <list>
#include <stdio.h>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/View.hpp>

#include "Entity.hpp"
#include "Cursor.hpp"
#include "../dynamics/collisions/CollidableCircle.hpp"
#include "../dynamics/Vector.hpp"
#include "projectiles/Projectile.hpp"
#include "../renderGraph/RenderNode.hpp"

struct CreatureAction;
struct CreatureWalkingDirectionChanger;

class Creature : public Entity, public CollidableCircle {
    friend CreatureWalkingDirectionChanger;
    friend CreatureAction;
private:
    enum Dir {
        U, L, D, R, NB_D
    };
    
    sf::CircleShape m_sprite;
    bool isPlayer = true;
    Command::Direction m_pressed;
    
    sf::RenderWindow *m_win;
    sf::View *m_view;
    Cursor *m_cursor;
    bool m_relativeWalk = false;
    float m_walkingAngle = 0.f;
    
    sf::Vector2f m_lookDirection = sf::Vector2f(0.f, 0.f);
    std::list<Dir> m_lastDirs;
    
public:
    Creature(VectorF, sf::RenderWindow&, sf::View&, Cursor&, bool = false, sf::Vector2f = sf::Vector2f(0.f, 0.f));
    void changeDirection();
    
    // Implements RenderNode :
    void drawThis(sf::RenderTarget&, sf::RenderStates) const;
    Category::Type getCategory() const;
    
    // Implements Collidable:
    bool touchedBy(const Entity&);
    
private:
    void computeWalkingAngle();
};



struct CreatureWalkingDirectionChanger {
    Command::Direction dir;
    bool release;
    
    CreatureWalkingDirectionChanger(Command::Direction d, bool b = false) : dir(d), release(b) {}
    
    void operator() (Creature& c, sf::Time) const {
        bool wasMoving = false;
        // Check if c was moving :
        if(!c.m_lastDirs.empty()) wasMoving = true;
        
        if(!release){
        switch (dir) {
            case Command::Up:
                c.m_lastDirs.push_front(c.U);
                break;
            case Command::Left:
                c.m_lastDirs.push_front(c.L);
                break;
            case Command::Down:
                c.m_lastDirs.push_front(c.D);
                break;
            case Command::Right:
                c.m_lastDirs.push_front(c.R);
                break;
            case Command::None: ;
        }
        }
        else {
            switch (dir) {
                case Command::Up:
                    c.m_lastDirs.remove(c.U);
                    break;
                case Command::Left:
                    c.m_lastDirs.remove(c.L);
                    break;
                case Command::Down:
                    c.m_lastDirs.remove(c.D);
                    break;
                case Command::Right:
                    c.m_lastDirs.remove(c.R);
                    break;
                case Command::None: ;
            }
        }
        
        
        if(!c.m_lastDirs.empty()) {
            c.computeWalkingAngle();
            if(!wasMoving) c.m_ease.in(true);
        }
        else c.m_ease.out(true);
    }
};

struct CreatureAction {
    void operator() (Creature& c, sf::Time) const {
        std::cout << "Pan!" << std::endl;

        c.attachChild(std::unique_ptr<Projectile>
            (new Projectile(c.m_lookDirection, c.getPosition(), c.isPlayer)));
    }
};