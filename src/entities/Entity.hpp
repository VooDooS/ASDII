//
//  Entity.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 09/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <SFML/graphics.hpp>
#include <stdio.h>

#include "../dynamics/Easings.hpp"
#include "../dynamics/Vector.hpp"
#include "../renderGraph/RenderNode.hpp"

class Entity : public RenderNode {
protected:
    VectorF m_velocity;
    
    Easings m_ease;
    sf::Time m_ease_time = sf::Time::Zero, m_ease_duration;
    
public:
    Entity(Easings::Kind = Easings::Kind::CONST, VectorF = VectorF(0.f, 0.f, 0.f), sf::Time = sf::seconds(0.5f), float = 0.f);
    VectorF getVelocity() const;

    virtual bool updateThis(sf::Time);
    
private:
    void updateEasing(sf::Time);
};
