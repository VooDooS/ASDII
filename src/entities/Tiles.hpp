//
//  Tiles.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 18/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <stdio.h>

#include "Entity.hpp"

class Tiles : public Entity {
private:
    
public:
    Tiles();
    
    
    // Implements RenderNode :
    void drawThis(sf::RenderTarget&, sf::RenderStates) const;
};