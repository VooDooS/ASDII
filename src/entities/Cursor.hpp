#pragma once

#include <SFML/Graphics.hpp>


#include "Entity.hpp"
class Cursor :
    public Entity
{
private:
    static const float c_size;
    sf::CircleShape m_pointer;
    sf::RenderWindow& m_win;

    sf::Vector2i m_mouse_coordinates;
public:
    Cursor(sf::RenderWindow&);

    void setPosition(const sf::Vector2f &);

    void drawThis(sf::RenderTarget&, sf::RenderStates) const;
    Category::Type getCategory() const;
};

