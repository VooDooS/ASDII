#include "Cursor.hpp"

#include <iostream>
#include <SFML/Window/Mouse.hpp>

using namespace std;


const float Cursor::c_size = 4.f;

Cursor::Cursor(sf::RenderWindow &w) : m_win(w), m_pointer(c_size)
{
    m_pointer.setFillColor(sf::Color::Red);
    sf::FloatRect bounds = m_pointer.getLocalBounds();
    m_pointer.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
}


void Cursor::setPosition(const sf::Vector2f &p) {
    
    // Cursor should never move outside of the window 
    sf::Vector2f pos = p;
    if(p.x < 0) pos.x = 0;
    if(p.x > m_win.getSize().x - c_size) pos.x =  m_win.getSize().x - c_size;
    if(p.y < 0) pos.y = 0;
    if(p.y > m_win.getSize().y - c_size) pos.y =  m_win.getSize().y - c_size;
    
    Transformable::setPosition(pos);
}


void Cursor::drawThis(sf::RenderTarget& t, sf::RenderStates s) const {
    t.draw(m_pointer, s);
}

Category::Type Cursor::getCategory() const {
    return Category::Cursor;
}