#pragma once
#include "../../dynamics/collisions/CollidableCircle.hpp"

class Creature;
class Projectile :
    public Entity, public CollidableCircle
{
private:
    sf::CircleShape m_sprite;
    sf::Vector2f m_origin;
    float m_distanceToDeath = 800.f;
    bool isAlly;
public:
    Projectile(sf::Vector2f, sf::Vector2f, bool = false);
    ~Projectile();

    // Implementing Collidable:
    bool touchedBy(const Entity&);

protected:
    // Implementing RenderNode:
    virtual void drawThis(sf::RenderTarget&, sf::RenderStates) const;
    virtual bool updateThis(sf::Time);
    virtual Category::Type getCategory() const;
};

