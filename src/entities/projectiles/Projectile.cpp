#include "Projectile.hpp"

#include "../Creature.hpp"

using namespace std;

Projectile::Projectile(sf::Vector2f d, sf::Vector2f origin, bool ia)
    : Entity(Easings::CONST, VectorF(d, 800.f)), m_origin(origin), m_sprite(10.f), isAlly(ia),
        CollidableCircle(10.f)
{
    sf::FloatRect bounds = m_sprite.getLocalBounds();
    m_sprite.setOrigin(bounds.width / 2.f, bounds.height / 2.f);
    setPosition(origin);
}


Projectile::~Projectile(){}

bool Projectile::updateThis(sf::Time t) {
    // If Projectile too far away, we destroy it:
    if (VectorF::computeNorm(getPosition() - m_origin) > m_distanceToDeath) {
        return true;
    }
    else {
       return Entity::updateThis(t);
    }
}

void Projectile::drawThis(sf::RenderTarget &target, sf::RenderStates states) const {
    // Projectile position is not relative to its parent
    states.transform = getTransform();

    target.draw(m_sprite, states);
}

Category::Type Projectile::getCategory() const {
    if (isAlly) return Category::AllyProjectile;
    else return Category::EnemyProjectile;
}

bool Projectile::touchedBy(const Entity &c){
    switch (c.getCategory()){
        case Category::EnemyCreature:
            return true;
        case Category::None:
        default:
            return false;
    }
}