//
//  Entity.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 09/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Entity.hpp"

#include <iostream>

using namespace std;

Entity::Entity(Easings::Kind e, VectorF v, sf::Time easeTime, float p)
: m_velocity(v), m_ease_duration(easeTime), m_ease(e, easeTime, p){
    m_ease.out();
}

VectorF Entity::getVelocity() const {
    return m_velocity;
}

void Entity::updateEasing(sf::Time ed){
    m_ease.reBind(ed);
}

bool Entity::updateThis(sf::Time dt) {
    
    // Computing velocity :
    VectorF v = m_ease(dt, m_velocity);
    
    move(v.getSfVector()*dt.asSeconds());

    return false;
}