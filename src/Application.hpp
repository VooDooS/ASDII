//
//  Application.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 29/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <SFML/Graphics.hpp>
#include <stdio.h>


#include "Player.hpp"
#include "resources/ResourcesManager.hpp"
#include "states/StateStack.hpp"


class Application {
private:
    static const sf::Time m_timePerFrame;
    
private:
    StateStack m_stateStack;
    
    sf::RenderWindow m_window;
    sf::RenderTexture m_texture;
    ResourcesManager m_resourcesManager;
    Stats m_stats;
    
    Player m_player;
    
    sf::Time m_totalTime = sf::Time::Zero;
    bool m_focused = true;
    
public:
    Application();
    void run();
    
private:
    void render(sf::Time);
    void registerStates();
    void processInputs();
    void update(sf::Time);
    void testClose();
    void drawFPS(sf::Time);
};