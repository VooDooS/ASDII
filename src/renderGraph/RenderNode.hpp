#pragma once

#include <SFML/Graphics.hpp>

#include <list>
#include <memory>
#include <vector>

#include "Command.hpp"
#include "../dynamics/collisions/Collidable.hpp"

class Entity;
namespace Category {
    enum Type {
        None = 0,
        Scene = 1 << 0,
        Creature = 1 << 1,
        PlayerCreature = 1 << 2,
        EnemyCreature = 1 << 3,
        AllyProjectile = 1 << 4,
        EnemyProjectile = 1 << 5,
        Cursor = 1 << 6
    };
}

struct Command;
class RenderNode : 
    public sf::Drawable, public sf::Transformable, private sf::NonCopyable
{
public:
    using rn_ptr = std::unique_ptr<RenderNode>;

private:
    std::vector<rn_ptr> m_children;
    std::list<Collidable*> m_solidChildren;
    RenderNode* m_parent;
    
    sf::Vector2f m_lastPos;

public:
    RenderNode();
    ~RenderNode();

    void attachChild(rn_ptr);
    rn_ptr detachChild(const RenderNode&);

    sf::Transform getUniverseTransform() const;
    sf::Vector2f getUniversePosition() const;
    
    virtual Category::Type getCategory() const;
    
    bool update(sf::Time);
    void onCommand(const Command&, sf::Time);
    void testCollisions();

protected:
    const RenderNode& getParent()const;
    void revertLastTransform();
    void selfDetach() const;

    virtual bool updateThis(sf::Time) { return false; };

private:
    void addSolidChildren(Collidable*);
    void removeSolidChildren(const Collidable*);
    
    virtual void draw(sf::RenderTarget&, sf::RenderStates) const;
    virtual void drawThis(sf::RenderTarget&, sf::RenderStates) const {};
};

