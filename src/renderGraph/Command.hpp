//
//  Command.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 15/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <functional>

#include "RenderNode.hpp"

class RenderNode;
struct Command {
public:
    enum Direction {
        None = 0 << 0,
        Up = 1 << 0,
        Down = 1 << 1,
        Left = 1 << 2,
        Right = 1 << 3,
    };
    
    //Command(Category = Category::None, std::function<void(RenderNode&, sf::Time)> = nullptr);
    
    unsigned category;
    std::function<void(RenderNode&, sf::Time)> action;
};


// A tool for not having to cast Entities each time we use a Command :
template <typename GameObject, typename Function>
static std::function<void(RenderNode&, sf::Time)> derivedAction(Function fn)
{
    return [=] (RenderNode& node, sf::Time dt)
    {
        // Check if cast is safe :
        if(dynamic_cast<GameObject*>(&node) != nullptr) {
            fn(static_cast<GameObject&>(node), dt);
        }
    };
}