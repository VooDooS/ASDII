#include "RenderNode.hpp"

#include <algorithm>
#include <iostream>
#include <vector>

#include "../entities/Entity.hpp"

using namespace std;

RenderNode::RenderNode() : m_parent(nullptr)
{
}


RenderNode::~RenderNode()
{
}

void RenderNode::attachChild(rn_ptr child)
{ 
    child->m_parent = this;
    
    // If this child may be collidable, we add it to the corresponding list:
    if(Collidable *c = dynamic_cast<Collidable*>(child.get())) {
        addSolidChildren(c);
    }
    
    // And eventually we add it to te children list:
    m_children.push_back(std::move(child));
    
}

RenderNode::rn_ptr RenderNode::detachChild(const RenderNode &node)
{
    // Deleting it from collision list:
    if(const Collidable *c = dynamic_cast<const Collidable*>(&node))
        removeSolidChildren(c);
        
    
    // Deleting it from children list :
    auto found = std::find_if(m_children.begin(), m_children.end(),
        [&](rn_ptr& p) -> bool
        {
            return p.get() == &node;
        });

    rn_ptr result = std::move(*found); 
    result->m_parent = nullptr;
    m_children.erase(found);
    
    return result;
}

sf::Transform RenderNode::getUniverseTransform() const
{
    //Getting back absolute transofrm by trans-class-recursively multipliying all the relatives transforms:
    if (m_parent == nullptr) {
        return getTransform();
    }
    else {
        return m_parent->getUniverseTransform() * getTransform();
    }
}

sf::Vector2f RenderNode::getUniversePosition() const {
    return getUniverseTransform() * sf::Vector2f();
}

Category::Type RenderNode::getCategory() const {
    return Category::Scene;
}

void RenderNode::onCommand(const Command& c, sf::Time dt) {
    // Is this node affected by the command ? (using bitwise and)
    if (c.category & getCategory()) {
        c.action(*this, dt);
    }
    
    // And we transmit the command :
    for (const auto& child : m_children)
        child->onCommand(c, dt);
}

void RenderNode::testCollisions() {
    // For each solid children, we test the collision with the others (and then forget them):
    auto sc = m_solidChildren;
    
    while(!sc.empty()) {
        Collidable* c1 = sc.front();
        sc.pop_front();
        
        // A collision might delete an object, so a foreach is not adapted.
        auto sc2 = sc;
        while (!sc2.empty()) {
            Collidable* c2 = sc2.front();
            sc2.pop_front();
            
            if(c1->isColliding(*c2)){
                // Collidable should always be Entities
                auto e1 = dynamic_cast<const Entity*>(c1);
                auto e2 = dynamic_cast<const Entity*>(c2);

                bool b1 = c1->touchedBy(*e2);
                bool b2 = c2->touchedBy(*e1);
                
                // Destroying destroyed Entities:
                if (b1) {
                    e1->selfDetach();
                    sc.erase(find(sc.begin(), sc.end(), c1));
                }
                if (b2) {
                    e2->selfDetach();
                    sc.erase(find(sc.begin(), sc.end(), c2));
                }
            }
        }
        
    }
}

bool RenderNode::update(sf::Time t) {
    m_lastPos = getPosition();

    // Some Entities may self-destruct while updating (out of screen for exemple)
    // We need to destroy them AFTER updating them, that's the purpose of the DeathList.
    
    if(!updateThis(t)) {
        list<RenderNode*> deathList;
        
        for (const rn_ptr& child : m_children) {
            if (child->update(t)) {
                deathList.push_front(child.get());
            }
        }
        
        for (auto c : deathList) {
            c->selfDetach();
        }
        return false;
    }
    // No need to update children of a soon-to-be dead node:
    else return true;
}

void RenderNode::addSolidChildren(Collidable *c) {
    // This method add the children to the elder :
    if(m_parent != nullptr) m_parent->addSolidChildren(c);
    else  m_solidChildren.push_back(c);
}

void RenderNode::removeSolidChildren(const Collidable *c) {
    // This method add the children to the elder :
    if(m_parent != nullptr) m_parent->removeSolidChildren(c);
    else m_solidChildren.erase(find(m_solidChildren.begin(), m_solidChildren.end(), c));
}

const RenderNode& RenderNode::getParent() const {
    return *m_parent;
}

void RenderNode::revertLastTransform() {
    setPosition(m_lastPos);
}

void RenderNode::selfDetach() const {
    m_parent->detachChild(*this);
}

void RenderNode::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
    // Chaining input and self transforms :
    states.transform *= getTransform();

    drawThis(target, states);

    for (const rn_ptr& child : m_children) {
        
        child->draw(target, states);
    }
}
