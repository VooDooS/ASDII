//
//  CommandsQueue.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 16/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "CommandsQueue.hpp"

#include <iostream>

using namespace std;

Command CommandsQueue::pop() {
    Command c = front();
    std::queue<Command>::pop();
    return c;
}