//
//  CommandsQueue.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 16/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <queue>
#include <stdio.h>

#include "Command.hpp"

// TODO Inherit from dt container is bad
class CommandsQueue : public std::queue<Command>
{
public:
    Command pop();
};