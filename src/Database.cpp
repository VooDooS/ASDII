#include <fstream>
#include <sys/stat.h>

#include "Database.hpp"
#include "resources/Resource.hpp"

using namespace std;

Database::Database(){}


Database::~Database(){}

json Database::readFromFile(string f) {
    string path = Resource::resourcePath() + f;

    cout << "Try opening" << path << endl;
    //Opening file and reading config :
    ifstream t(path);
    stringstream buffer;
    buffer << t.rdbuf();
    t.close();

    return json::parse(buffer);
}

void Database::saveToFile(string f, json j) {
    ofstream ofs;
    ofs.open(Resource::resourcePath() + f, ofstream::trunc);

    ofs << j.dump(4);

    ofs.close();
}