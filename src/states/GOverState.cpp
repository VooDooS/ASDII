//
//  GOverState.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 02/03/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "GOverState.hpp"

#include <SFML/Graphics.hpp>

#include "../resources/Resource.hpp"

GOverState::GOverState(StateStack& s, Context c) : State(s, c) {}

bool GOverState::handleEvent(const sf::Event& event) {
    if (event.type == sf::Event::KeyPressed || event.type == sf::Event::MouseButtonPressed) {
        stackPop();
        stackPush(State::Game);
    }
    
    return true;
}

bool GOverState::update(sf::Time dt) {
    return true;
}
void GOverState::draw() {
    sf::Text text;
    sf::Font font;
    assert(font.loadFromFile(Resource::resourcePath() + "fonts/FantasqueSansMono-Bold.ttf"));
    text.setFont(font);
    
    text.setColor(sf::Color::Red);
    text.setString("YOU LOSE");
    
    text.setCharacterSize(80);
    
    // Center :
    sf::FloatRect textRect = text.getLocalBounds();
    text.setOrigin(textRect.left + textRect.width/2.0f,
                   textRect.top  + textRect.height/2.0f);
    text.setPosition(m_context.texture->getSize().x / 2., m_context.texture->getSize().y / 2.);
    m_context.texture->draw(text);
}