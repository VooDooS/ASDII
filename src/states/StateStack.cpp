//
//  StateStack.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 28/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "StateStack.hpp"

#include <cassert>

using namespace std;

// public //
StateStack::StateStack(State::Context c) : m_context(c) {}

void StateStack::pushState(State::ID sid) {
    m_pendingChanges.push_back(PendingChanges {Push, sid});
}
void StateStack::popState() {
    m_pendingChanges.push_back(PendingChanges {Pop});
}
void StateStack::clearStates() {
    m_pendingChanges.push_back(PendingChanges {Clear});
}

void StateStack::handleEvent(const sf::Event& e) {
    // Going reverse :
    for(auto itr = m_stack.rbegin(); itr != m_stack.rend(); ++itr) {
        // Passing the event and stop propagating if requested by the state :
        if(!itr->get()->handleEvent(e)) return;
    }
    
    applyPendingChanges();
}

void StateStack::update(sf::Time dt){
    // Going reverse :
    for(auto itr = m_stack.rbegin(); itr != m_stack.rend(); ++itr) {
        // Updating state and stop propagating if requested by it :
        if(!itr->get()->update(dt)) return;
    }
    
    applyPendingChanges();
}

void StateStack::draw(){
    // Going reverse :
    for(auto itr = m_stack.rbegin(); itr != m_stack.rend(); ++itr) {
        itr->get()->draw();
    }
}

bool StateStack::isEmpty() const {
    return m_stack.empty();
}

// private //
State::ptr StateStack::createState(State::ID sid) {
    assert(m_factories.find(sid) != m_factories.end());
    
    return m_factories[sid]();
}

void StateStack::applyPendingChanges() {
    for (auto change : m_pendingChanges) {
        switch (change.actionId) {
            case Push:
                m_stack.push_back(createState(change.stateId));
                break;
                
            case Pop:
                m_stack.pop_back();
                break;
                
            case Clear:
                m_stack.clear();
                
            default:
                break;
        }
    }
    
    m_pendingChanges.clear();
}