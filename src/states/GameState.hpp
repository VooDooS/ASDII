//
//  GameState.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 29/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <stdio.h>

#include "State.hpp"
#include "../Universe.hpp"

class GameState : public State {
private:
    Universe m_universe;
public:
    GameState(StateStack&, Context);
    
    virtual bool handleEvent(const sf::Event&);
    virtual bool update(sf::Time);
    virtual void draw();
    
};
