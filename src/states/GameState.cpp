//
//  GameState.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 29/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "GameState.hpp"

#include "State.hpp"
#include "../Player.hpp"


GameState::GameState(StateStack& s, Context c) : State(s, c), m_universe(c) {}

bool GameState::handleEvent(const sf::Event& event) {
    CommandsQueue& c = m_universe.getCommandsQueue();
    
    m_context.player->handleEvent(event, c);
    
    return true;
}

bool GameState::update(sf::Time dt) {
    m_universe.update(dt);
    
    m_context.player->handleRealtimeInput(m_universe.getCommandsQueue());
    
    return true;
}
void GameState::draw() {
    m_universe.draw();
}