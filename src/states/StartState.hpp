//
//  StartState.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 02/03/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <stdio.h>

#include "State.hpp"

class StartState : public State {
public:
    StartState(StateStack&, Context);
    
    virtual bool handleEvent(const sf::Event&);
    virtual bool update(sf::Time);
    virtual void draw();
};