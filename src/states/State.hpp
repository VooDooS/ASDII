//
//  State.hpp
//  ASDII
//
//  Created by Ulysse Gérard on 28/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <memory>
#include <SFML/System/Time.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <stdio.h>


struct Stats {
    int score = 0;
    int best = 0;
};

class Player;
class ResourcesManager;
class StateStack;
class State {
public:
    enum ID {
        Start,
        Game,
        GameOver,
        Pause
    };
    using ptr = std::unique_ptr<State>;
    struct Context {
        // A holder for shared resources between States
        Context(sf::RenderWindow&, sf::RenderTexture&, ResourcesManager&, Player&, Stats&);
        
        sf::RenderWindow *win;
        sf::RenderTexture *texture;
        ResourcesManager *rm;
        Player *player;
        Stats *stats;
    };
    
private:
    StateStack *m_stack;
protected:
    Context m_context;
    
public:
    State(StateStack&, Context);
    virtual ~State();
    
    virtual bool handleEvent(const sf::Event&) = 0;
    virtual bool update(sf::Time) = 0;
    virtual void draw() = 0;
    
protected:
    void stackPush(ID);
    void stackPop();
    void stateClear();
    Context getContext() const;
};
