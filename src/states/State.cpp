//
//  State.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 28/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "State.hpp"

#include "StateStack.hpp"

State::Context::Context(sf::RenderWindow& rw, sf::RenderTexture& t, ResourcesManager& rmi, Player& p, Stats &s) {
    win = &rw;
    texture = &t;
    rm = &rmi;
    player = &p;
    stats = &s;
}

State::State(StateStack& s, Context c) : m_stack(&s), m_context(c){}

State::~State() {}

void State::stackPush(State::ID sid) {
    m_stack->pushState(sid);
}

void State::stackPop() {
    m_stack->popState();
}

void State::stateClear() {
    m_stack->clearStates();
}