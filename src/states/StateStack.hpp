//
//  StateStack.hpp
//  ASDII
//
//  State Managment mostly inspired by SFML Game Development book
//
//  Created by Ulysse Gérard on 28/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#pragma once

#include <functional>
#include <map>
#include <SFML/Window/Event.hpp>
#include <SFML/System/NonCopyable.hpp>
#include <SFML/System/Time.hpp>
#include <stdio.h>
#include <vector>

#include "State.hpp"

class StateStack : private sf::NonCopyable {
public:
    enum Action {
        Push, Pop, Clear
    };
    
private:
    struct PendingChanges {
        Action actionId;
        State::ID stateId;
    };
    
private:
    std::vector<State::ptr> m_stack;
    std::vector<PendingChanges> m_pendingChanges;
    State::Context m_context;
    std::map<State::ID, std::function<State::ptr()>> m_factories;
    
public:
    StateStack(State::Context);
    
    template <typename T>
    void registerState(State::ID sid) {
        m_factories[sid] = [this] () {
            return State::ptr(new T(*this, m_context));
        };
    }
    
    void pushState(State::ID);
    void popState();
    void clearStates();
    
    void handleEvent(const sf::Event&);
    void update(sf::Time);
    void draw();
    
    bool isEmpty() const;
    
private:
    State::ptr createState(State::ID);
    // We need that because changes can't happen when iterating (cf Death of entities)
    void applyPendingChanges();
};