#pragma once

#include <map>
#include "resources/RTile.hpp"

class Cell
{
private:
    const RTile& m_tile;
public:
    Cell(const RTile&);
    ~Cell();
};

using Coord = std::pair<int, int>;
using CoordCell = std::pair<Coord, Cell>;
using Cells = std::map<std::pair<int, int>, Cell>;