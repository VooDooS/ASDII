//
//  Player.cpp
//  ASDII
//
//  Created by Ulysse Gérard on 16/02/2016.
//  Copyright © 2016 Destructoïds. All rights reserved.
//

#include "Player.hpp"
#include "entities/Creature.hpp"
#include "entities/Cursor.hpp"
#include "renderGraph/RenderNode.hpp"
#include <iostream>

using namespace std;

Player::Player(sf::RenderWindow& w) : m_win(&w) {
    // We initialize the Action and Command maps :
    m_actionMap[sf::Keyboard::Z] = MoveUp;
    m_actionMap[sf::Keyboard::Q] = MoveLeft;
    m_actionMap[sf::Keyboard::S] = MoveDown;
    m_actionMap[sf::Keyboard::D] = MoveRight;
    
    m_commandMap[MoveUp].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Up));
    m_commandMap[MoveLeft].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Left));
    m_commandMap[MoveDown].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Down));
    m_commandMap[MoveRight].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Right));
    
    for (auto& p : m_commandMap) {
        p.second.category = Category::PlayerCreature;
    }
    
    m_commandOnReleaseMap[MoveUp].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Up, true));
    m_commandOnReleaseMap[MoveLeft].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Left, true));
    m_commandOnReleaseMap[MoveDown].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Down, true));
    m_commandOnReleaseMap[MoveRight].action = derivedAction<Creature>(CreatureWalkingDirectionChanger(Command::Right, true));
    
    for (auto& p : m_commandOnReleaseMap) {
        p.second.category = Category::PlayerCreature;
    }
}

void Player::handleRealtimeInput(CommandsQueue& cq) {
    
    
    // Mouse move => Cursor move
    
    // Delta-movement of the mouse :
    auto mousePos = sf::Mouse::getPosition(*m_win);
    auto center = sf::Vector2i(m_win->getSize() / 2u);
    auto deltaMouse = center - mousePos;
    sf::Mouse::setPosition(center, *m_win);
    
    // Sending the command to Cursor :
    struct Command setPos = {Category::Cursor, derivedAction<Cursor>
    ([=](Cursor& cur, sf::Time)
     {
         auto pos = sf::Vector2f(cur.getPosition() - (sf::Vector2f(deltaMouse)));
         cur.setPosition(pos);
     })};
    cq.push(setPos);
    
    // Changing player creature direction :
    struct Command setDir = {Category::PlayerCreature, derivedAction<Creature>
        ([=](Creature& crea, sf::Time)
         {
             /*auto pc = m_win->mapPixelToCoords(crea.m_c, m_win->getView());
             cout << "m " << mousePos.x << "; " << mousePos.y << endl;
             cout << pc.x << "; " << pc.y << endl;*/
             crea.changeDirection();
         })};
    cq.push(setDir);
}

void Player::handleEvent(const sf::Event & e, CommandsQueue & cq) {
    switch (e.type) {
    case sf::Event::KeyPressed:
    {
        const auto bind = m_actionMap.find(e.key.code);
        if (bind != m_actionMap.end() && !isRealTimeAction(bind->second)) {
            cq.push(m_commandMap[bind->second]);
        } break;
    }
    case sf::Event::KeyReleased:
    {
        const auto bind = m_actionMap.find(e.key.code);
        if (bind != m_actionMap.end() && !isRealTimeAction(bind->second)) {
            cq.push(m_commandOnReleaseMap[bind->second]);
        } break;
    }
    case sf::Event::MouseButtonPressed:
    {
        cq.push(Command{
            Category::PlayerCreature,
            derivedAction<Creature>(CreatureAction())
        });
        break;
    }
    default: break;
    }
}

bool Player::isRealTimeAction(Action a) const {
    switch (a) {
        default:
            return false;
    }
}