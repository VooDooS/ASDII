uniform sampler2D tex;
uniform float dx;
uniform float dy;

void main()
{
    vec2 coord = vec2(dx*floor(gl_TexCoord[0].x/dx),
                      dy*floor(gl_TexCoord[0].y/dy));
    gl_FragColor = texture2D(tex, coord);
}